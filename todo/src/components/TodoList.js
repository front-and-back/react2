import React, { Component } from 'react'

export class Todolist extends Component {

    render() {
        const  items= this.props.items;
        const  listItems= items.map(item => {
            return(
                <li key={item.id}>{item.text}</li>
            )
        })
        return (
            <div>
                <ul style={{listStyle : 'none'}}>
                    {listItems}
                </ul>
            </div>
        )
    }
}

export default Todolist
