import React, { Component } from 'react'
import TodoList from './TodoList'

export class Todo extends Component {
    state= {
        name:"Todo List",
        items:[],
        text:""
    }
    handelChange = (e)=> {
        this.setState({
            text : e.target.value
        })
    }
    handelSubmit= (e) => {
    e.preventDefault();
    if(this.state.text.length === 0){return ;}
    const newItem ={
        text: this.state.text,
        id: Date.now()
    }
    this.setState(state => ({
        items :state.items.concat(newItem),
        text:""
    }))
    }
    render() {
        return (
            <div>
                <h1>{this.state.name}</h1>
                <form onSubmit={this.handelSubmit}>
                    <input type="text" onChange={this.handelChange} value={this.state.text}/>
                    <button>Add Todo #{this.state.items.length + 1}</button>
                </form>
                <TodoList items={this.state.items}/>
            </div>
        )
    }
}

export default Todo
