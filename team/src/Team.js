import React, { Component } from 'react'
import TeamItem from './TeamItem'

export class Team extends Component {
    state = {
        items:[
            {id:"1", name:"Rafeef", email:"rafeef@hotmail.com",img:"/images/01.jpg"},
            {id:"2", name:"Ahamed" ,email:"Ahamed@hotmail.com",img:"/images/02.jpg"},
            {id:"3", name:"Hind" ,email:"Hind@hotmail.com",img:"/images/03.jpg"}
        ]
    }
    render() {
        return (
            <div>
                <TeamItem items={this.state.items} />
            </div>
        )
    }
}

export default Team
