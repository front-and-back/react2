import React, { Component } from 'react'

export class TeamItem extends Component {
   
    render() {
        const items = this.props.items;
        const TheItem = items.map((item) => {
            return (
                <div className="col-md-4" key={item.id}>
                    <div className="card mt-5">
                        <div className="card-header">
                        <img src={item.img}  style={{maxWidth: '100%'}}/>
                        </div>
                        <div className="card-body">
                            <h2>{item.name}</h2>
                            <h5>{item.email}</h5>
                        </div>
                    </div>
                </div>
            )
        });
        return (
            <div class="row">
                {TheItem}
            </div>
        )
    }
}

export default TeamItem
