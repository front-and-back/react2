import React, { Component } from 'react'
import Social from './component/Social'
import Text from './component/Text'
import Header from './component/Header'
import Home from './component/Home'
import Contact from './component/Contact'
import About from './component/About'
import { BrowserRouter as Router  , Route  } from 'react-router-dom'

export class App extends Component {
  state = {
    first: 'rafeef',
    last: 'Aqraa'
  }
  render() {
    return (
      <Router>
      <div className="container">
      <Header />
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/contact">
        <Contact />
      </Route>
      <Route path="/about">
        <About />
      </Route>
      <Route path="/Social">
        <Social />
      </Route>
      </div>
      </Router>
    )
  }
}

export default App


