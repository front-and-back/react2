import React from 'react'
import SocialItem from './SocialItem'

export default function Social() {
    return (
        <div>
            <ul className="list-group">
            <SocialItem media="FaceBook" id="facebbok" img="https://picsum.photos/60"/>
            <SocialItem media="Youtube" id="youtube" img="https://picsum.photos/61"/>
            <SocialItem media="Instagram" id="instagram" img="https://picsum.photos/63"/>
            <SocialItem media="Linkedin" id="linkedin" img="https://picsum.photos/64"/>
        </ul>
        </div>
    )
}
