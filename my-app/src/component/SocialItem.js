import React from 'react'

export default function SocialItem(props) {
    return (
        <div>
            <li className="list-group-item">
                    <img src={props.img} alt={props.id}/>
                    <input type="checkbox" id={props.id} name={props.id}  />
                    <label htmlFor={props.id}>{props.media}</label>
            </li>
        </div>
    )
}
